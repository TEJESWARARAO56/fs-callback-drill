const fs = require('fs');

// Read the given file lipsum.txt
fs.readFile('./lipsum.txt', 'utf8', (err, data) => {
    if (err) {
        console.error(`Error reading file lipsum.txt: ${err.message}`);
        return;
    }
    // Convert the content to uppercase & write to a new file
    const upperCaseContent = data.toUpperCase();
    const newFileName = 'lipsum_uppercase.txt';
    const newFilePath = `./${newFileName}`;

    fs.writeFile(newFilePath, upperCaseContent, (err) => {
        if (err) {
            console.error(`Error writing file ${newFilePath}: ${err.message}`);
            return;
        }
        console.log(`New file ${newFilePath} created successfully.`);

        // Store the name of the new file in filenames.txt
        fs.appendFile('filenames.txt', `${newFileName}\n`, (err) => {
            if (err) {
                console.error(`Error writing to filenames.txt: ${err.message}`);
                return;
            }
            console.log(`New file filenames.txt created successfully.`);
            // Read the new file and convert it to lower case. 
            fs.readFile(newFilePath, 'utf8', (err, data) => {
                if (err) {
                    console.error(`Error reading file lipsum.txt: ${err.message}`);
                    return;
                }
                // Then split the contents into sentences. Then write it to a new file
                const lowerCaseContent = data.toLowerCase().replace(/\. /g, '\n').trim();
                const lowerCaseContentfilename = 'lowerLipsum.txt'
                fs.writeFile(`./${lowerCaseContentfilename}`, lowerCaseContent, (err) => {
                    if (err) {
                        console.error(`Error writing file ${newFilePath}: ${err.message}`);
                        return;
                    }
                    console.log(`New file ${lowerCaseContentfilename} created successfully.`);
                    // Store the name of the new file in filenames.txt
                    fs.appendFile('filenames.txt', `${lowerCaseContentfilename}\n`, (err) => {
                        if (err) {
                            console.error(`Error writing to filenames.txt: ${err.message}`);
                            return;
                        }
                        // Read the new files, sort the content,
                        fs.readFile('lipsum_uppercase.txt', 'utf8', (err, upperCaseData) => {
                            if (err) {
                                console.error(`Error reading file lipsum.txt: ${err.message}`);
                                return;
                            }
                            fs.readFile('lowerLipsum.txt', 'utf8', (err, lowerCaseData) => {
                                if (err) {
                                    console.error(`Error reading file lipsum.txt: ${err.message}`);
                                    return;
                                }
                                const sortedUpperText = (upperCaseData).split("\n").sort().join("\n").trim()
                                const sortedLText = (lowerCaseData).split("\n").sort().join("\n").trim();
                                const sortedUppertextfilename = 'sortedUpperLipsum.txt'
                                const sortedLowertextfilename = 'sortedLowerLipsum.txt'
                                // write it out to a new file.
                                fs.writeFile(`./${sortedUppertextfilename}`, sortedUpperText, (err) => {
                                    if (err) {
                                        console.error(`Error writing file ${newFilePath}: ${err.message}`);
                                        return;
                                    }
                                    console.log(`New file ${sortedUppertextfilename} created successfully.`);

                                    fs.writeFile(`./${sortedLowertextfilename}`, sortedLText, (err) => {
                                        if (err) {
                                            console.error(`Error writing file ${newFilePath}: ${err.message}`);
                                            return;
                                        }
                                        console.log(`New file ${sortedLowertextfilename} created successfully.`);

                                        // Store the name of the new file in filenames.txt
                                        fs.appendFile('filenames.txt', `${sortedUppertextfilename}\n`, (err) => {
                                            if (err) {
                                                console.error(`Error writing to filenames.txt: ${err.message}`);
                                                return;
                                            }
                                            fs.appendFile('filenames.txt', `${sortedLowertextfilename}\n`, (err) => {
                                                if (err) {
                                                    console.error(`Error writing to filenames.txt: ${err.message}`);
                                                    return;
                                                }

                                                fs.readFile('filenames.txt', 'utf8', (err, fileNamesData) => {
                                                    if (err) {
                                                        console.error(`Error reading file lipsum.txt: ${err.message}`);
                                                        return;
                                                    }
                                                    // Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.*/
                                                    (fileNamesData.trim(" ") + "\nfilenames.txt").split('\n').forEach((filename) => {

                                                        fs.unlink(filename, (err) => {
                                                            if (err) {
                                                                console.error(err);
                                                                return;
                                                            }
                                                            console.log(`File ${filename} was deleted`);
                                                        });
                                                    })
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
