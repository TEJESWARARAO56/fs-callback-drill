const fs = require('fs');
const path = require('path');

const dirPath = './json_files';
const numFiles = 5;

// Create directory if it doesn't exist
function createAndDeleteFilesInFolder(dirPath) {
    fs.mkdir(dirPath, { recursive: true }, function (err) {
        if (err) {
            console.log(err)
        } else {
            console.log("New directory successfully created.")
        }
    })

    // Generate random JSON files
    for (let i = 0; i < numFiles; i++) {
        const filename = `file_${i}.json`;
        const filepath = path.join(dirPath, filename);

        const data = {
            id: i,
            name: `Random Name ${i}`,
            value: Math.random() * 100
        };

        const jsonData = JSON.stringify(data, null, 2);

        fs.writeFile(filepath, jsonData, (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log(`File ${filename} created`);
            }
            fs.unlink(filepath, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log(`File ${filepath} was deleted`);
            });
        });
    }
}
createAndDeleteFilesInFolder(dirPath);


/*
    Folder structure:
        ├── problem1.js
        ├── problem2.js
        └── test
            ├── testProblem1.js
            └── testProblem2.js
*/

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously
*/

/*
    Problem 2:

    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/